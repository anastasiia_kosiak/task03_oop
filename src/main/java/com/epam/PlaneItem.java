package com.epam;

/**
 * Model class
 */
public class PlaneItem  implements Comparable<PlaneItem> {
        private  PlaneModel planeModel;
        private int numberOfSeats;
        private int cargoCapacity;
        private int fuel;
        private Integer distance;

        public PlaneItem(PlaneModel planeModel, int numberOfSeats,
                         int cargoCapacity, int fuel, Integer distance) {
            this.planeModel = planeModel;
            this.numberOfSeats = numberOfSeats;
            this.cargoCapacity = cargoCapacity;
            this.fuel = fuel;
            this.distance = distance;
        }
        public int getFuel(){
            return this.fuel;
        }
        public Integer getDistance(){
            return this.distance;
        }
        public int getNumberOfSeats(){
            return this.numberOfSeats;
        }
        public int getCargoCapacity(){
            return this.cargoCapacity;
        }
        public PlaneModel getPlaneModel(){
            return this.planeModel;
        }


        @Override
        public String toString(){
            return "Distance = "+distance+".";
        }
        public int compareTo(PlaneItem plane) {
            return this.getDistance().compareTo(plane.getDistance());
        }
}
