package com.epam;
import java.util.Random;
public enum PlaneModel {
    BOEING_787, ER_300, A_380, BOEING_777, JETS;

    public static PlaneModel getPlaneModel() {
        Random rand = new Random();
        return values()[rand.nextInt(values().length)];
    }
}
