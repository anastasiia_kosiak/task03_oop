package com.epam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * View class
 */
public class MenuView {
    private Map<String, String> menu;
    private Map<String, Print> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    CompanyModel model = new CompanyModel();

    public MenuView() {
        mainMenu();
    }

    public void mainMenu(){

        menu = new LinkedHashMap<String, String>();
        menu.put("1", "  1 -> Show available flights");
        menu.put("2", "  2 -> Sort planes by distance");
        menu.put("3", "  3 -> Find planes by fuel capacity");
        menu.put("Q", "  Q -> exit menu");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::showAllData);
        methodsMenu.put("2", this::sortByDistance);
        methodsMenu.put("3", this::findByFuelCapacity);


    }

    public void enterFuel(){
        System.out.println("Enter your fuel count:");
    }

    public void showAllData(){
        model.printPlaneInfo();
    }

    public void sortByDistance(){
        model.sortByDistance();
    }

    public void findByFuelCapacity(){
        enterFuel();
        int pos = 0;
        pos = menuTotalFuel();
        model.searchByFuel(pos);
    }

    public int menuTotalFuel(){
        int userPrice = 0;
        try {
            while (userPrice <= 0 ){
                try {
                    BufferedReader inPrice = new BufferedReader(new InputStreamReader(System.in));
                    userPrice = Integer.parseInt(inPrice.readLine());
                }catch (NumberFormatException c){}
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return userPrice;
    }
    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }
    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, enter an option.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
