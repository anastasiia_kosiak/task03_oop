package com.epam;
import java.util.*;

/**
 * Controller class
 */
public class CompanyModel {
    private PlaneParameters plane = new PlaneParameters();
    private List<PlaneItem> companyPlanes = new ArrayList<PlaneItem>();
    private int numberOfPlanes = 10;

    public CompanyModel() {
        createAirCompany();
    }

    private void createAirCompany() {
        PlaneItem planeItem;
        for (int i = 0; i < numberOfPlanes; ++i) {
            planeItem = new PlaneItem(PlaneModel.getPlaneModel(),
                    plane.calculateNumberOfSeats(),
                    plane.calculateCargoCapacity(),
                    plane.calculateFuel(), plane.calculateDistance());
            companyPlanes.add(planeItem);
        }
    }

    public void printPlaneInfo() {
        int seatCount = 0;
        int cargoCount = 0;
        for (Iterator<PlaneItem> iter = companyPlanes.iterator(); iter.hasNext(); ) {
            PlaneItem item = iter.next();
            seatCount += item.getNumberOfSeats();
            cargoCount += item.getCargoCapacity();
            printPlaneList(item);
        }
        System.out.println("Total number of seats: " + seatCount);
        System.out.println("Total cargo capacity: " + cargoCount);
    }

    private void printPlaneList(PlaneItem item) {
        System.out.println("Plane model: " + item.getPlaneModel());
        System.out.println("Number of seats: " + item.getNumberOfSeats());
        System.out.println("Cargo capacity: " + item.getCargoCapacity());
        System.out.println("Fuel capacity: " + item.getFuel());
        System.out.println("Distance: " + item.getDistance());
        System.out.println();
    }

    private void printIterator(List<PlaneItem> itr) {
        for (Iterator<PlaneItem> it = itr.iterator(); it.hasNext(); ) {
            PlaneItem currentPlane = it.next();
            printPlaneList(currentPlane);
        }
    }

    public void sortByDistance() {
        Collections.sort(companyPlanes);
        printIterator(companyPlanes);
    }

    public void searchByFuel(int fuel) {
        for (PlaneItem item : companyPlanes) {
            if (item.getFuel() <= fuel) {
                printPlaneList(item);
            }
        }
    }

    public void findByModel(int type) {
        for (Iterator<PlaneItem> it = companyPlanes.iterator(); it.hasNext(); ) {
            PlaneItem item = it.next();
            if (item.getPlaneModel() == PlaneModel.values()[type - 1]) {
                printPlaneList(item);
            }
        }
    }
}
