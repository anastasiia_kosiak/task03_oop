package com.epam;
import java.util.Random;
public class PlaneParameters {
        public int calculateFuel(){
            Random rand = new Random();
            int fuel = rand.nextInt(400);
            return fuel;
        }

        public int calculateNumberOfSeats(){
            Random rand = new Random();
            int seats = rand.nextInt(300)+100;
            return seats;
        }

        public int calculateCargoCapacity(){
            Random rand = new Random();
            int cargo = rand.nextInt(200)+50;
            return cargo;
        }

        public Integer calculateDistance(){
            Random rand = new Random();
            int distance = rand.nextInt(2300)+1000;
            return distance;
        }
    }
