package com.epam;

@FunctionalInterface
public interface Print {
    void print();
}